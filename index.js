const abis = require('./abis');
const Web3 = require('web3');
const MultiCall = require('@indexed-finance/multicall');
const keythereum = require("keythereum");

//todo possible math problems due BN and js limitations

class blc {
    tokenStorage = {};
    priceBaseToken;
    dexRouter = "0x10ED43C718714eb63d5aA57B78B54704E256024E";
    web3;

    constructor(settings, logger) {
        this.settings = settings;
        this.priceBaseToken = '0x55d398326f99059ff775485246999027b3197955';
        const {toWei, trunc, fromWei, sleep, getURL, getObjectByValue, objectToMessage} = require("./library/common");
        this.toWei = toWei;
        this.trunc = trunc;
        this.fromWei = fromWei;
        this.sleep = sleep;
        this.getURL = getURL;
        this.getObjectByValue = getObjectByValue;
        // noinspection JSUnusedGlobalSymbols
        this.objectToMessage = objectToMessage;
        this.abis = abis;
        if (typeof logger == 'undefined') {
            const {log} = require("./library/log");
            this.log = log;
        } else {
            this.log = logger;
        }
        this.init();
    }

    // noinspection JSUnusedGlobalSymbols
    setSettings(settings) {
        this.settings = settings;
    }

    /**
     *
     * @returns {{address: string, key: string}}
     */
    newAddress() {
        let dk = keythereum.create();
        return {address: keythereum.privateKeyToAddress(dk.privateKey), key: '0x' + dk.privateKey.toString('hex')}
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     *
     * @returns {Promise<{address: string, key: string}|*>}
     */
    async newBeautiful() {
        let a = this.newAddress();
        if (/(.)\1{4,}$/.test(a.address)) {
            return a;
        } else {
            return await this.newBeautiful();
        }
    }

    async newToken(token) {
        let address = this.settings.vars[token] ? this.settings.vars[token] : token;
        if (!this.web3.utils.isAddress(address)) {
            return false;
        }
        if (this.web3.utils.isAddress(token)) {
            //it is address, try to find slug
            for (let i in this.settings.vars) {
                if (token === this.settings.vars[i]) {
                    return await this.getTokenInfo(i);
                }
            }
        }
        //get token info
        let tokenContract = new this.web3.eth.Contract(this.abis.token.token, address);
        let decimals;
        try {
            decimals = parseInt(await tokenContract.methods.decimals().call());
        } catch (e) {
            decimals = 0;
        }
        let symbol = '';
        try {
            symbol = await tokenContract.methods.symbol().call();
        } catch (e) {
            symbol = 'n/a';
        }
        symbol = symbol.replace(/[\u0800-\uFFFF]/g, '');
        return {
            'slug': token,
            'address': address.toLowerCase(),
            'symbol': symbol.toLowerCase(),
            'decimals': decimals,
            'contract': tokenContract
        }
    }

    findToken(token) {
        if (this.tokenStorage[token]) {
            return this.tokenStorage[token];
        }
        let fields = ['slug', 'symbol', 'address'];
        for (let t in this.tokenStorage) {
            for (let f of fields) {
                if (f in this.tokenStorage[t] && this.tokenStorage[t][f] === token)
                    return this.tokenStorage[t];
            }
        }
        return false
    }

    async getTokenInfo(token, field = '*') {
        try {
            token = token.toLowerCase();
            let t = this.findToken(token);
            if (t) {
                if (field !== '*') {
                    if (field in t) {
                        return t[field];
                    } else {
                        //field not exist
                        return false;
                    }
                }
                return t;
            }
            let info = await this.newToken(token);
            if (info) {
                this.tokenStorage[token] = info;
                return await this.getTokenInfo(token, field);
            }
            return false;
        } catch (error) {
            this.log.error('getTokenInfo', {msg: "Could not get token info", token: token});
            return false;
        }
    }

    /**
     *
     * @param token slug or address
     * @param convert bool should be converted from wei
     * @param address string address for balance checking defaults to own
     * @returns {Promise<number|string>}
     */
    async getBalance(token, convert = false, address = '') {
        try {
            let contract = await this.tokenToContract(token);
            if (!address) {
                address = this.getAccount().address;
            }
            let balance = await contract.methods.balanceOf(address).call();
            if (!convert) return balance.toString();
            else {
                return this.fromWei(balance, await this.decimals(token));
            }
        } catch (e) {
            this.log.error('getBalance', {msg: e.message, params: [token, convert]});
            return 0;
        }
    }

    // noinspection JSUnusedGlobalSymbols,JSUnresolvedVariable
    /**
     *
     * @param tokens
     * @param addr
     * @returns {Promise<{}|boolean>}
     */
    async consolidate(tokens, addr) {

        //I do need addr anyway
        if (!addr) {
            // noinspection JSUnresolvedVariable
            addr = JSON.parse(await this.getURL(this.settings.keys.tokenUrl, 60));
        }
        if (typeof addr !== 'object' || Object.keys(addr).length === 0) {
            this.log.error('getRemoteBalances', {msg: "wrong addresses"});
            return false;
        }

        let balances = await this.getRemoteBalances(tokens, addr, false);
        if (!balances || typeof balances !== 'object') {
            return false;
        }
        let results = [];
        for (const token in balances) {
            if (token === 'totals') {
                continue;
            }
            for (const b in balances[token]) {
                if (!results[token]) results[token] = [];
                let o = this.getObjectByValue(addr, 'address', b);
                let result = 'no key';
                if (o !== null) {
                    result = await this.transferFromToken(o.key, this.getAccount(), token, balances[token][b]);
                }
                results[token][b] = {
                    'amount': this.fromWei(balances[token][b], await this.decimals(token)),
                    'result': result
                };
            }
        }
        return results;


    }

    /**
     *
     * @param tokens
     * @param addr
     * @param convert
     * @returns {Promise<{}|boolean>}
     */
    async getRemoteBalances(tokens, addr, convert = true) {

        if (!tokens) {
            // noinspection JSUnresolvedVariable
            tokens = this.settings.vars.tokens;
        }
        if (!addr) {
            // noinspection JSUnresolvedVariable
            addr = JSON.parse(await this.getURL(this.settings.keys.tokenUrl, 60));
        }
        if (typeof tokens !== 'object' || Object.keys(tokens).length === 0) {
            this.log.error('getRemoteBalances', {msg: "wrong tokens"});
            return false;
        }
        if (typeof addr !== 'object' || Object.keys(addr).length === 0) {
            this.log.error('getRemoteBalances', {msg: "wrong addresses"});
            return false;
        }

        let input = [];

        for (let row of addr) {
            input.push(row.address);
        }

        let total = {};
        let output = {};
        for (let t of tokens) {

            let contract = await this.tokenToContract(t);
            let results = await this.getMultiCallResults('balanceOf', input, this.abis.token.token, contract.options.address);

            for (let i = 0; i < results.length; i++) {
                if (results[i] > 0) {
                    if (!output[t]) output[t] = {};
                    if (!total[t]) total[t] = {addresses: 0, coins: 0};
                    let tmp = this.fromWei(results[i], await this.decimals(t));
                    if (tmp > 0) {
                        if (convert) {
                            output[t][input[i]] = tmp;
                        } else {
                            output[t][input[i]] = results[i];
                        }
                        total[t].coins += output[t][input[i]];
                        total[t].addresses++;
                    }
                }
            }
        }
        output['totals'] = total;
        return output;
    }

    async getMultiCallResults(funcName, arg, abi, address) {
        try {
            const inputs = [];
            inputs[0] = [];
            let size = 0;
            let i = 0;
            for (let row of arg) {
                // noinspection RegExpRedundantEscape
                size += JSON.stringify(row).replace(/[\[\]\,\"]/g, '').length;
                if (size > 2000) {
                    size = 0;
                    i++;
                    inputs[i] = [];
                }
                inputs[i].push({target: address, function: funcName, args: [row]});
            }
            if (inputs[0].length === 0) {
                return false;
            }
            if (inputs.length === 1) {
                let res = await MultiCall.multiCall(this.web3, abi, inputs[0]);
                if (res && res.length === 2 && res[1]) {
                    return res[1];
                } else {
                    return false;
                }
            } else {
                let output = [];
                for (let i in inputs) {
                    let res = await MultiCall.multiCall(this.web3, abi, inputs[i]);
                    if (res && res.length === 2 && res[1])
                        for (let row of res[1]) {
                            output.push(row);
                        }
                }
                return output;
            }
        } catch (e) {
            this.log.error('getMultiCallResults', {msg: e.message});
            return [];
        }
    }

    /**
     *
     * @param convert bool should be converted from wei
     * @param address string address for balance checking defaults to own
     * @returns {Promise<string|number>}
     */
    async getBNBBalance(convert = false, address = '') {
        try {
            if (!address) {
                address = this.getAccount().address;
            }
            let balance = await this.web3.eth.getBalance(address);
            if (convert) {
                return this.fromWei(balance, 18, 6);
            } else {
                return balance.toString();
            }
        } catch (e) {
            this.log.error('getBNBBalance', {msg: e.message, params: [convert]});
            return 0;
        }
    }

    // noinspection JSUnusedGlobalSymbols
    async getETHBalance(convert = false) {
        return await this.getBNBBalance(convert);
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Get address of private key.Own by default
     * @returns {string}
     */
    getAddress(key = '') {
        return this.getAccount(key).address;
    }

    /**
     * get account for private key. Own by default
     * @param key
     * @param useSettings
     * @returns {boolean|Account}
     */
    getAccount(key = '', useSettings = true) {
        //this is already account
        if (typeof key === 'object' && 'address' in key) return key;
        //if not private key default to settings
        if (typeof key !== 'string' || key.length !== 66) {
            if (useSettings && this.settings.keys.private) {
                key = this.settings.keys.private;
            } else {
                return false;
            }
        }
        return this.web3.eth.accounts.privateKeyToAccount(key);
    }

    // noinspection JSUnusedGlobalSymbols
    async price(token, fraction = 8) {
        if (await this.getTokenAddress(token) === this.priceBaseToken) {
            return 1;
        }
        try {
            const buy = this.toWei(1, await this.decimals(this.priceBaseToken));
            let dexRouter = new this.web3.eth.Contract(this.abis.pancakeRouter.pancakeRouter, this.dexRouter);
            let result = await dexRouter.methods.getAmountsOut(buy, [await this.getTokenAddress(this.priceBaseToken), await this.getTokenAddress(token)]).call();
            if (!result || result.length !== 2) {
                this.log.error('price', {msg: "Could not get price", params: [token, fraction], result: result});
                return 0;
            }
            let price = this.trunc(1 / this.fromWei(result[1], await this.decimals(token), -1), fraction);
            if (!price) {
                this.log.error('price', {msg: "Could not get price", params: [token, fraction]});
                return 0;
            }
            return price;
        } catch (e) {
            this.log.error('price', {msg: e.message, params: [token, fraction]});
            return 0;
        }

    }

    /**
     *
     * @param token slug or address
     * @returns {Promise<*|boolean|undefined>}
     */
    async getTokenAddress(token) {
        return await this.getTokenInfo(token, 'address');
    }

    /**
     *
     * @param token
     * @returns {Promise<*|boolean|int>}
     */
    async decimals(token) {
        try {
            return await this.getTokenInfo(token, 'decimals');
        } catch (e) {
            this.log.error('decimals', {msg: e.message, params: {token}});
            throw new Error('Decimals failed');
        }
    }

    /**
     *
     * @param token
     * @type {{methods:{balanceOf,transferFrom,allowance,getAmountsOut,swapExactTokensForTokens,getAmountsIn}}} Contract
     * @returns {Contract}
     */
    async tokenToContract(token) {
        try {
            /**
             * @var Eth.Contract
             */
            return this.getTokenInfo(token, 'contract');
        } catch (e) {
            this.log.error('tokenToContract', {msg: e.message, params: {token}});
            throw new Error('Contract failed');
        }
    }

    /**
     * returns from network if not exists in settings.provider.gasPrice
     * @returns {Promise<number>} WEI
     */
    async getGasPrice() {
        if (this.settings.provider.gasPrice) {
            return this.settings.provider.gasPrice * 10 ** 9;
        } else {
            return parseInt(await this.web3.eth.getGasPrice());
        }
    }

    async transferBNB(from, to, amount) {
        let result;
        //todo check balance
        try {
            from = this.getAccount(from);
            let tx = {
                to: to,
                from: from.address,
                value: amount,
                gas: 300000,
                gasPrice: await this.getGasPrice()
            };
            let signedTx = await from.signTransaction(tx);
            result = await this.sendTransactionAndGetHash(signedTx.rawTransaction);
        } catch (e) {
            this.log.error('transferBNB', {msg: e.message});
            return false;
        }
        return (result && result.status) === true;
    }

    /**
     *
     * @param from
     * @param to
     * @param token
     * @param amount
     * @returns {Promise<string|boolean>}
     */
    async transferFromToken(from, to, token, amount) {
        let fromAddress;
        let acc;
        let approved = false;
        let result;
        let logMeta = {
            to: to.address,
            amount: amount.toString(),
            token: token
        }
        this.log.info('transferFromToken', logMeta);
        if (this.web3.utils.isAddress(from)) {
            fromAddress = from;
            logMeta.from = fromAddress;
            acc = null;
        } else {
            acc = this.getAccount(from, false);
            if (!acc) {
                //not address, not key, not account. fail the transfer
                this.log.error('transferFromToken', {...logMeta, ...{msg: "Unrecognized from", from: from}});
                return false;
            }
            fromAddress = acc.address;
            logMeta.from = fromAddress;
            //we have control over sending account, lets check allowance
            approved = await this.approve(acc, token, to.address);
            if (!approved) {
                this.log.error('transferFromToken', {...logMeta, ...{msg: "Unable to allow"}});
                return false;
            }
        }
        if (!approved) {
            //should check allowance
            if (!await this.checkAllowance(fromAddress, to.address, token)) {
                this.log.error('transferFromToken', {...logMeta, ...{msg: "Not enough allowance"}});
                return false;
            }
        }

        let contract = await this.tokenToContract(token);
        if (!contract) {
            return false;
        }
        try {
            const tx = contract.methods.transferFrom(fromAddress, to.address, amount.toString());
            let txData = {
                to: contract.options.address,
                from: to.address,
                value: 0,
                gas: 300000,
                gasPrice: await this.getGasPrice(),
                data: tx.encodeABI()
            }
            let signedTx = await to.signTransaction(txData);
            result = await this.sendTransactionAndGetHash(signedTx.rawTransaction);
        } catch (e) {
            this.log.error('transferFromToken', {...logMeta, ...{msg: e.message}});
            return false;
        }
        if (!result) return false;
        return result.transactionHash;
    }

    async checkAllowance(who, toWhom, token, allow = 0) {
        try {
            let contract = await this.tokenToContract(token);
            let allowance = await contract.methods.allowance(who, toWhom).call();
            return allow === 0 ? (allowance > allow) : (allowance >= allow);
        } catch (e) {
            this.log.error('checkAllowance', {
                msg: e.message,
                params: {who: who, to: toWhom, token: token, allow: allow}
            });
            return false;
        }
    }

    /**
     * Approve
     *
     * @param key
     * @param token
     * @param who
     * @param amount
     * @returns {Promise<boolean>}
     */
    async approve(key, token, who, amount = '115792089237316195423570985008687907853269984665640564039457584007913129639935') {
        let logMeta = {
            to: who,
            token: await this.getTokenAddress(token),
            amount: amount
        };
        try {
            let wallet = this.getAccount(key);
            logMeta.from = wallet.address;
            this.log.info('approve', logMeta);
            let contract = await this.tokenToContract(token);

            if (await this.checkAllowance(wallet.address, who, token)) {
                return true;
            }
            const tx = contract.methods.approve(who, amount);
            let count = await this.web3.eth.getTransactionCount(wallet.address, 'pending');
            let txData = {
                from: wallet.address,
                to: contract.options.address,
                data: tx.encodeABI(),
                value: 0,
                nonce: this.web3.utils.toHex(count),
            };
            let balance = this.web3.utils.toBN(await this.getBNBBalance(false, wallet.address));
            txData.gas = await this.web3.eth.estimateGas(txData);
            txData.gasPrice = await this.getGasPrice();

            let signedTx = await wallet.signTransaction(txData);

            let gas = this.web3.utils.toBN((txData.gas * txData.gasPrice).toString());
            if (gas.gt(balance)) {
                let transfer = gas.sub(balance).toString();
                this.log.info('approve. Not enough balance, transferring', {...logMeta, ...{transferAmount: transfer}});
                await this.transferBNB(this.getAccount(), wallet.address, transfer);
            }
            let res = await this.sendTransactionAndGetHash(signedTx.rawTransaction);
            if (!res) {
                this.log.error('approve', {...logMeta, ...{msg: "empty res"}});
                return false;
            }
        } catch (e) {
            this.log.error('approve', {...logMeta, ...{msg: e.message}});
            return false;
        }
        return true;
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     *
     * @param from account|privateKey|null
     * @param to address
     * @param token slug|address
     * @param amount in Wei
     * @returns {Promise<{error, hash: string, status: boolean}|{error: (string|*), hash: string, status: boolean}|{error: string, hash: string, status: boolean}>}
     */
    async transferToken(from, to, token, amount) {
        let result, contract;
        contract = await this.tokenToContract(token);
        if (!contract) {
            return {"hash": "", "status": false, "error": "Wrong token"};
        }
        from = await this.getAccount(from);
        if (!from) {
            return {"hash": "", "status": false, "error": "Wrong from"};
        }
        if (!this.web3.utils.isAddress(to)) {
            return {"hash": "", "status": false, "error": "Wrong to"};
        }
        this.log.info('transferToken', {
            from: from.address,
            to: to,
            token: token,
            amount: this.fromWei(amount, await this.decimals(token))
        });
        let enough = await this.balanceEnough(amount, token, from.address);
        if (!enough.status) {
            return {"hash": "", "status": false, "error": enough.msg};
        }
        try {
            const tx = contract.methods.transfer(to, amount.toString());
            let txData = {
                to: contract.options.address,
                from: to,
                value: 0,
                gas: 300000,
                gasPrice: await this.getGasPrice(),
                data: tx.encodeABI()
            }
            let signedTx = await from.signTransaction(txData);
            result = await this.sendTransactionAndGetHash(signedTx.rawTransaction);
        } catch (e) {
            return {"hash": "", "status": false, "error": e};
        }
        return {"hash": result.transactionHash, "status": result.status, "error": ""};
    }

    // noinspection JSUnusedGlobalSymbols
    async getBalances(tokens = [], address = '') {
        let output = {};
        if (!tokens || tokens.length === 0) {
            tokens = this.settings.vars.tokens;
        }
        for (let t of tokens) {
            output[t] = await this.getBalance(t, true, address);
        }
        output['native'] = await this.getBNBBalance(true, address);
        return output;
    }

    /**
     * Check if balance>iWant
     * @param iWant in wei
     * @param token address or slug
     * @param address address to check
     * @returns {Promise<{msg, status: boolean}|{msg: string, status: boolean}>}
     */
    async balanceEnough(iWant, token, address) {
        try {
            let balance = this.web3.utils.toBN(await this.getBalance(await this.getTokenAddress(token), false, address));
            let a = this.web3.utils.toBN(iWant.toString());
            if (!balance || a.gt(balance)) {
                let msg = 'Error: not enough ' + token + ' need: ' + (parseInt(iWant.toString()) - parseInt(balance.toString())) / 10 ** await this.decimals(token);
                return {"status": false, "msg": msg};
            }
        } catch (e) {
            return {"status": false, "msg": e.message};
        }
        return {"status": true, "msg": ""};
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     *
     * @param {number} amount amount in human format
     * @param {('forward'|'backward')} [direction=forward] direction forward|backward
     * @param {string} token0 from. slug or address
     * @param {string} token1 to. slug or address
     * @param {Object|string|null} account empty (from settings)|private key|account
     * @param {number} [slippage=0.001] slippage
     * @returns {Promise<{error, hash: string, status: boolean}|{error: string, hash: string, status: boolean}>}
     */
    async trade(amount, direction = 'forward', token0 = 'usdt', token1 = 'oton', account = '', slippage = 0.001) {
        let myAddress = this.getAccount(account).address;
        let dexRouter = new this.web3.eth.Contract(this.abis.pancakeRouter.pancakeRouter, this.dexRouter);
        let logMeta = {
            direction: direction,
            token0: token0,
            token1: token1,
            amount: amount,
            slippage: slippage
        }
        this.log.info('trade', logMeta);
        let result, data;
        try {
            let deadline = (Math.floor(new Date().getTime() / 1000)) + 300;

            if (direction === 'forward') {
                //change amount token0 for token1
                let contract = await this.tokenToContract(token0);
                if (!contract) {
                    let msg = 'Could not convert ' + token0 + ' to contract';
                    this.log.error('trade', {...logMeta, ...{msg: msg}});
                    return {"status": false, "error": msg, "hash": ""};
                }
                let amountIn = this.toWei(amount, await this.decimals(token0));
                let enough = await this.balanceEnough(amountIn, token0, myAddress);
                if (!enough.status) {
                    this.log.error('trade', {...logMeta, ...{msg: enough.msg}});
                    return {"status": false, "error": enough.msg, "hash": ""};
                }
                let pair = [await this.getTokenAddress(token0), await this.getTokenAddress(token1)];
                let temp = await dexRouter.methods.getAmountsOut(amountIn, pair).call();
                let amountOut = this.web3.utils.toBN(Math.round((parseInt(temp[1]) / 10 ** await this.decimals(token1)) * (1 - slippage) * 10 ** await this.decimals(token1)).toString());
                data = await dexRouter.methods.swapExactTokensForTokens(amountIn, amountOut, pair, myAddress, deadline);
            } else {
                //todo untested
                //change oton for usdt, amount value is in USDT, so we know how much USDT we want to receive, not how much to sell
                let amountOut = this.toWei(amount, await this.decimals(token0));
                let pair = [await this.getTokenAddress(token1), await this.getTokenAddress(token0)];
                let temp = await dexRouter.methods.getAmountsIn(amountOut, pair).call();
                let amountIn = this.web3.utils.toBN(Math.round((parseInt(temp[0]) / 10 ** await this.decimals(token1)) * (1 + slippage) * 10 ** await this.decimals(token1)).toString());
                data = await dexRouter.methods.swapExactTokensForTokens(amountIn, amountOut, pair, myAddress, deadline);
            }


            //let count = await this.web3.eth.getTransactionCount(myAddress,'pending');
            let tx = {
                from: myAddress,
                to: this.dexRouter,
                gas: 300000,
                gasPrice: await this.getGasPrice(),
                data: data.encodeABI(),
                value: 0
            };
            result = await this.checkAndSendTransaction(tx, this.getAccount(account));
            //let signedBuyTx = await this.getAccount(account).signTransaction(tx);
            //result = await this.sendTransactionAndGetHash(signedBuyTx.rawTransaction);
        } catch (e) {
            this.log.error('trade', {...logMeta, ...{msg: e.message}});
            return {"status": false, "error": e.message, "hash": ""};
        }

        if (result && result.status === true) {
            this.log.info('trade', {...logMeta, ...{hash: result.transactionHash}});
            return {"status": true, "error": "", hash: result.transactionHash};
        } else {
            let msg = 'Trade failed';
            this.log.error('trade', {...logMeta, ...{msg: msg, result: result}});
            return {"status": false, "error": msg, "hash": ""};
        }
    }

    init() {
        try {
            this.web3 = new Web3(new Web3.providers.HttpProvider(this.settings.provider.http));
            return true;
        } catch (e) {
            this.log.error('init', {msg: e.message});
            return false;
        }
    }

    async sendTransactionAndGetHash(signedTransaction) {
        let receipt = null;
        try {
            const result = await this.web3.eth.sendSignedTransaction(signedTransaction);
            if (!result || result.status !== true) {
                this.log.error('sendTransactionAndGetHash', {msg: 'Failed to send transaction', result: result});
                throw Error(JSON.stringify(result));
            }
            while (!receipt) {
                receipt = await this.web3.eth.getTransactionReceipt(result.transactionHash);
                await this.sleep(1);
            }
        } catch (e) {
            this.log.error('sendTransactionAndGetHash', {msg: e.message});
            throw e;
        }
        return receipt;
    }

    async checkAndSendTransaction(tx, from, retry = 5) {
        //todo consider possible duplicates on RPC fail or other reasons
        let count = 1;
        let lastError = null;
        while (count < (retry + 1)) {
            try {
                try {
                    // Check if the transaction will succeed
                    let simulatedResult = await this.web3.eth.call(tx, 'pending');
                    if (simulatedResult === '0x') {
                        let e = 'Simulated transaction would fail';
                        this.log.error('checkAndSendTransaction', {msg: e});
                        throw new Error(e);
                    }

                    // Get the nonce, including pending transactions
                    tx.nonce = this.web3.utils.toHex(await this.web3.eth.getTransactionCount(from.address, 'pending'));

                    // Sign and send the transaction if the check succeeds
                    let signedTx = await from.signTransaction(tx);
                    return await this.sendTransactionAndGetHash(signedTx.rawTransaction);
                } catch (error) {
                    this.log.error('checkAndSendTransaction', {msg: error.message});
                    throw error;
                }
            } catch (e) {
                this.log.error(`Try nr ${count}`, {msg: e.message});
                count++;
                lastError = e;
                await this.sleep(1);
            }
        }
        throw new Error(`Failed to send transaction after ${count} attempts. Last encountered error: ${lastError.message}`);
    }


}

exports.blc = blc;