const pancakeRouter = require('./pancakeRouter.json');
const pancakeFactory = require('./pancakeFactory.json');
const pancakePool = require('./pancakePool.json');
const token = require('./token.json');
const otkup = require('./otkup.json');

module.exports = {
    pancakeRouter,
    pancakeFactory,
    pancakePool,
    token,
    otkup
};
