const {createLogger, format, transports} = require("winston");
require('winston-daily-rotate-file');

let transport = new transports.DailyRotateFile({
    filename: './logs/%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d'
});

let log = createLogger({
    format: format.combine(format.timestamp(), format.json()),
    transports: [transport, new transports.Console()],
    exceptionHandlers: [new transports.File({filename: "./logs/exceptions.log"})],
    rejectionHandlers: [new transports.File({filename: "./logs/rejections.log"})],
});

module.exports = {log};