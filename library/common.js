const crypto = require("crypto");
const fs = require("fs");
const axios = require("axios");
const BigNumber = require('bignumber.js');

/**
 *
 * @param amount string|number|BN
 * @param dec decimals
 * @returns {string}
 */
function toWei(amount, dec) {
    let i = 0;
    if (amount.toString().indexOf('.') !== -1) {
        i = amount.toString().length - (amount.toString().indexOf('.') + 1);
    }
    let res = amount.toString().split('.').join('');
    if (dec < i) {
        res = res.substring(0, res.length - (i - dec));
    } else {
        res = res + "0".repeat(dec - i);
    }
    return res;
}

/**
 *
 * @param amount in WEI
 * @param dec decimals
 * @param truncate digits after point
 * @returns {number}
 */
function fromWei(amount, dec, truncate = 8) {
    amount = amount.toString();
    while (dec >= amount.length) {
        amount = '0' + amount;
    }
    let cnt = 0;
    let res = '';
    for (let i = amount.length - 1; i >= 0; i--) {
        if (cnt === dec) {
            res = '.' + res;
        }
        res = amount.charAt(i) + res;
        cnt++;
    }
    res = parseFloat(res);
    if (truncate > -1) {
        res = trunc(res, truncate);
    }
    return res;
}

function trunc(amount, dec) {
    if (typeof amount === 'string') {
        amount = new BigNumber(amount);
    } else if (typeof amount === 'number') {
        amount = new BigNumber(amount);
    }

    const factor = new BigNumber(10).exponentiatedBy(dec);
    const truncated = amount.times(factor).integerValue(BigNumber.ROUND_DOWN).div(factor);

    return truncated.toNumber();
}

/**
 * Simple sleep function
 * @param seconds
 * @returns {Promise<void>}
 */
async function sleep(seconds) {
    await new Promise(resolve => setTimeout(resolve, seconds * 1000));
}

/**
 * Fetches remote URL and stores in cache for ttl seconds
 * @param url
 * @param ttl
 * @param post
 * @param headers
 * @returns {Promise<string|boolean|any>}
 */
async function getURL(url, ttl = 10000, post = {}, headers = {}) {
    let hash = crypto.createHash('md5').update(url).digest('hex');
    let dir = './cache/' + hash.substring(0, 2);
    if (!fs.existsSync(dir)) {
        try {
            fs.mkdirSync(dir, {recursive: true});
        } catch (e) {
            return false;
        }

    }
    const path = dir + '/' + hash + '.html';
    if (fs.existsSync(path) && ttl > 0) {
        let stats = fs.statSync(path);
        let seconds = (new Date().getTime() - stats.mtime) / 1000;
        seconds = seconds < 0 ? 0 : seconds;
        if (seconds < ttl && ttl > 0) {
            return fs.readFileSync(path, 'utf8')
        }
    }
    let result;
    try {
        if (Object.keys(post).length === 0) {
            result = await axios.get(url, {headers: headers, transformResponse: []});
        } else {
            result = await axios.post(url, post, {headers: headers, transformResponse: []});
        }
    if (result && result.data) {
        if (ttl > 0) {
            fs.writeFileSync(path, result.data);
        }
        return result.data;
    }
    } catch (e) {
        return false;
    }
    return false;

}

/**
 * Attempt to format json and object for telegram
 * @param obj
 * @returns {string|*}
 */
function objectToMessage(obj) {
    if (typeof obj === 'string') {
        try {
            obj = JSON.parse(obj);
        } catch (e) {
            return obj;
        }
    }
    if (typeof obj !== 'object') {
        return obj;
    }
    let res = '';
    for (const key in obj) {
        res = res + "<b>" + key.toUpperCase() + "</b>:\t" + ((typeof obj[key] === "object") ? "%0A\n===%0A\n" : " ") + objectToMessage(obj[key]) + ((Object.keys(obj)[Object.keys(obj).length - 1]) === key ? "" : "%0A\n");
    }
    return res;
}

/**
 * @example
 * const map = [{"address":"qqq","key":"xxxx","coin":"usdt"},{"address":"sss","key":"vvvvv","coin":"usdt"}];
 * getObjectByValue(map,"key","vvvvvv");
 * @returns {object|null}
 * @param o
 * @param k
 * @param v
 */
function getObjectByValue(o, k, v) {
    let result = o.find(obj => {
        return obj[k] === v
    })
    return (typeof result === 'object') ? result : null;
}


module.exports = {toWei, fromWei, trunc, sleep, getURL, objectToMessage, getObjectByValue}
